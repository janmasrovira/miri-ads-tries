module Trie where

import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as Map
import           Data.Maybe
import           TrieCommon

-- Set of strings
data Trie = Node {
  subforest_ :: HashMap Char Trie
  }
  | Leaf
  deriving (Show)

empty :: Trie
empty = Node Map.empty

fromList :: [String] -> Trie
fromList = foldr (insert . addEndChar) empty

size :: Trie -> Int
size Leaf = 1
size (Node h) = Map.foldr (\t k-> k + size t) 0 h

countNodes :: FilePath -> IO Int
countNodes file = nodeCount . fromList . words <$> readFile file

countWords :: FilePath -> IO Int
countWords file = size . fromList . words <$> readFile file

nodeCount :: Trie -> Int
nodeCount Leaf = 1
nodeCount (Node h) = 1 + Map.foldr (\t k-> k + nodeCount t) 0 h

avgHeight :: Trie -> Double
avgHeight = avg . go 0
  where
    go h Leaf = [h]
    go h (Node m) = concatMap (go (h + 1)) (Map.elems m)
      where

insert :: String -> Trie -> Trie
insert ((==[endChar]) -> True) (Node f) = Node (Map.insert endChar Leaf f)
insert (x:xs) (Node f) = Node f'
  where
    f' = Map.alter falt x f
    falt :: Maybe Trie -> Maybe Trie
    falt m = Just $ insert xs (fromMaybe empty m)
insert xs _ = error $ "this should not happen, insert _ _. " ++ xs

contains :: Trie -> String -> Bool
contains (Node _) "" = False
contains (Node f) (x:xs) = maybe False (`contains`xs) (Map.lookup x f)
contains Leaf "" = True
contains Leaf _ = error "this should not happen"

toList :: Trie -> [String]
toList = map init . go ""
  where
    go pref Leaf = [pref]
    go pref (Node s) = concat [ go (pref ++ [x]) t | (x, t) <- Map.toList s]
