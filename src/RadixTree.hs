module RadixTree where

import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as Map
import           Data.Maybe
import           TrieCommon

data RTrie = Node {
  _mask :: String
  , _subforest :: HashMap Char RTrie
  }
  | Leaf -- end of a string
  deriving (Show)

-- a -> b -> (common, rest of a, rest of b)
match :: String -> String -> (String, String, String)
match a b = (comm, drop l a, drop l b)
  where comm = map fst $ takeWhile (uncurry (==)) $ zip a b
        l = length comm

empty :: RTrie
empty = Node [] Map.empty

singleton :: String -> RTrie
singleton = flip insert empty

fromList :: [String] -> RTrie
fromList = foldr (insert . addEndChar) empty

-- strings must be ended by a special char.
-- a mask cannot contain such character.
insert :: String -> RTrie -> RTrie
insert ((== [endChar]) -> True) (Node m s) =
  case m of
    "" -> Node m (Map.insert endChar Leaf s)
    (hm:ms) -> insert [endChar] $ Node "" (Map.singleton hm (Node ms s))
insert "" _ = Leaf
insert xs@(_:_) (Node m s)
  | Map.null s = Node (init xs) (Map.singleton endChar Leaf)
  | otherwise =
    case match xs m of
      (_, "", _) -> error "does the string end with endChar?"
      -- splits the mask: xs/mask: a$ abc
      (c, rx:rxs, hm:ms) -> Node c s'
        where
          s' = Map.fromList [(hm, Node ms s), (rx, singleton rxs)]
      -- consumes mask and continues: xs/mask: abcde$ abc
      (_, rx:rxs, "") -> Node m s'
        where
          s' = Map.alter falt rx s
          falt :: Maybe RTrie -> Maybe RTrie
          falt mt = Just $ insert rxs (fromMaybe empty mt)
insert _ Leaf = error "inserting a nonempty string reached a leaf"

toList :: RTrie -> [String]
toList = map init . go ""
  where go pref Leaf = [pref]
        go pref (Node m s) = concat [ go (pref ++ m ++ [x]) t | (x, t) <- Map.toList s]

size :: RTrie -> Int
size = length . toList

nodeCount :: RTrie -> Int
nodeCount Leaf = 1
nodeCount (Node _ h) = 1 + Map.foldr (\t k-> k + nodeCount t) 0 h

avgHeight :: RTrie -> Double
avgHeight = avg . go 0
  where
    go h Leaf = [h]
    go h (Node _ m) = concatMap (go (h + 1)) (Map.elems m)
      where

countWords :: FilePath -> IO Int
countWords file = size . fromList . words <$> readFile file

countNodes :: FilePath -> IO Int
countNodes file = nodeCount . fromList . words <$> readFile file
