module Benchmarks.IntSet where

import           Control.Monad.Random
import           Criterion.Main
import qualified Data.HashSet as H
import qualified Data.IntSet as I
import           Data.List.Split
import qualified Data.Set as S

seed :: Int
seed = 0

many :: Int
many = 99999

seqList :: [Int]
seqList = [0..many]

seqLists :: [[Int]]
seqLists = chunksOf (max 1 $ many`div`30) seqList

randLists :: [[Int]]
randLists = chunksOf (max 1 $ many`div`30) randList

randList :: [Int]
randList = take many $ randomRs (0, u) (mkStdGen seed)
  where
    u :: Int
    u = many`div`10

main :: IO ()
main = defaultMain [
  bgroup "insert/seq" [
      intset seqList
      , set seqList
      , hashset seqList
      ]
  , bgroup "insert/rand" [
      intset randList
      , set randList
      , hashset randList
      ]
  , bgroup "union/seq" [
      uintset seqLists
      , uset seqLists
      , uhashset seqLists
      ]
  , bgroup "union/rand" [
      uintset randLists
      , uset randLists
      , uhashset randLists
                 ]

  ]
  where intset = bench "Patricia" . whnf (I.size . I.fromList)
        set = bench "BalancedBin" . whnf (S.size . S.fromList)
        hashset = bench "HashSet" . whnf (H.size . H.fromList)
        uintset = bench "Patricia" . whnf (I.size . I.unions . map I.fromList)
        uset = bench "BalancedBin" . whnf (S.size . S.unions . map S.fromList)
        uhashset = bench "HashSet" . whnf (H.size . H.unions . map H.fromList)
