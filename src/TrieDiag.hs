{-# LANGUAGE FlexibleContexts, NoMonomorphismRestriction #-}
module TrieDiag where

import           Data.Colour.Names
import qualified Data.HashMap.Strict as Map
import           Data.Tree
import           Diagrams.Backend.Cairo
import           Diagrams.Prelude
import           Diagrams.TwoD.Layout.Tree
import           DiagramsHelper
import           System.Directory
import           System.FilePath
import qualified Trie as T
import           TrieCommon

diagPath :: FilePath
diagPath = "diagrams" </> "trie"

mkDiagDir :: IO ()
mkDiagDir = createDirectoryIfMissing True diagPath

inDiags :: FilePath -> FilePath
inDiags = (diagPath </>)

toTree :: T.Trie -> Tree String
toTree = go ""
  where
    go lab T.Leaf = Node lab []
    go lab (T.Node s) = Node lab [go [c] t | (c, t) <- Map.toList s]

fromListD :: [String] -> [Diagram B]
fromListD = map (drawTrie . toTree) . scanl (flip T.insert) T.empty

go :: IO ()
go = mkDiagDir >> renderManyCairo' (\i -> inDiags $ "trie-" ++ show i <.> "png")
  (fromListD $ map addEndChar l4)

drawTrie :: Tree String -> Diagram B
drawTrie t =
  renderTree drawNode
             (~~)
             (symmLayout' (with & slHSep .~ 4 & slVSep .~ 4) t)
  # centerXY # pad 1.1
  where
    drawNode ((==[endChar]) -> True) = square 1 # fc black
    drawNode s = (<> circle 1.2 # fc white) . text $ s
