{-# LANGUAGE FlexibleContexts, NoMonomorphismRestriction #-}
module RadixTreeDiag where

import           Data.Colour.Names
import qualified Data.HashMap.Strict as Map
import           Data.Tree
import           Diagrams.Backend.Cairo
import           Diagrams.Prelude
import           Diagrams.TwoD.Layout.Tree
import           DiagramsHelper
import qualified RadixTree as T
import           System.Directory
import           System.FilePath
import           TrieCommon

diagPath :: FilePath
diagPath = "diagrams" </> "radix"

mkDiagDir :: IO ()
mkDiagDir = createDirectoryIfMissing True diagPath

inDiags :: FilePath -> FilePath
inDiags = (diagPath </>)

toTree :: T.RTrie -> Tree String
toTree = go ""
  where
    go lab T.Leaf = Node lab []
    go lab (T.Node m s) = Node (lab ++ m) [go [c] t | (c, t) <- Map.toList s]

fromListD :: [String] -> [Diagram B]
fromListD = map (drawTrie . toTree) . scanl (flip T.insert) T.empty . map addEndChar

go :: IO ()
go = goGen fromListD

go2 :: IO ()
go2 = goGen fromListPairD

goGen :: ([String] -> [Diagram B]) -> IO ()
goGen fromListF = mkDiagDir >> renderManyCairo' (\i -> inDiags $ "radix-" ++ show i <.> "png")
  (fromListF l3)

drawTrie :: Tree String -> Diagram B
drawTrie t =
  renderTree drawNode
             (~~)
             (symmLayout' (with & slHSep .~ 4 & slVSep .~ 4) t)
  # centerXY # pad 1.1
  where
    drawNode ((==[endChar]) -> True) = square 1 # fc black
    drawNode s = (<> rect (len + 0.2) 1.8 # fc white) . text $ s
      where len = fromIntegral $ length s


fromListPairD :: [String] -> [Diagram B]
fromListPairD = map (\(a, b) -> hsep 1 [a, b]). consecPairs . fromListD

consecPairs :: [b] -> [(b, b)]
consecPairs [] = []
consecPairs l = zip l (tail l)
