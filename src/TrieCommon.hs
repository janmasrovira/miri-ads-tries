module TrieCommon where

endChar :: Char
endChar = '$'

addEndChar :: String -> String
addEndChar = (++ [endChar])

l1 :: [String]
l1 = ["canto", "cantes", "canta", "cantem", "canteu", "canten"
     , "cantare", "cantaras", "cantara", "cantarem", "cantareu", "cantaran"
     , "cantava", "cantaves", "cantava", "cantavem", "cantaveu", "cantaven"]

l3 :: [String]
l3 = ["romane", "romanus", "romulus", "rubens", "ruber", "rubicon", "rubicundus"]

l4 = ["dog", "dogma", "doll"]



avg :: [Double] -> Double
avg l = sum l / (fromIntegral $ length l)

