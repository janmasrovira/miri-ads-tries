\documentclass[xcolor=dvipsnames]{beamer}
\usetheme{Madrid}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[]{colortbl}
\usepackage[T1]{fontenc} % Change according your file encoding
\usepackage[]{booktabs}
\usepackage{graphicx}
\usepackage{url}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{listings}
\usepackage{latexsym,amsmath,amssymb,enumerate,commath,complexity}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\setbeamertemplate{theorems}[numbered]
\newtheorem{conjecture}{Conjecture}
\newtheorem*{theorem*}{Theorem}
\makeatletter
\graphicspath{{figures/}}
\definecolor{Gray}{gray}{0.85}
\newcolumntype{a}{>{\columncolor{Gray}}r}
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
    \usebeamerfont{date in head/foot}\today\hspace*{2em}
    \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother
\begin{document}

\title[Tries]
{Tries}
\author[Mas Rovira, Jan]
{Jan Mas Rovira\inst{1}}
\institute{
  \inst{1}%
  Computer Science, FIB
}
\date{\today}
\subject{Computer Science}
\frame{\titlepage}

\begin{frame}
	\frametitle{Table of Contents}
	\tableofcontents
\end{frame}

\section{String Tries}


\begin{frame}[fragile]
  \frametitle{Tries}
  A \textcolor{red}{trie} is a tree-like ordered data structure that allows the dynamic storage
  of a set of string keys.
  \\~\\
  Good for
  \begin{itemize}
  \item Lookup prefixes (autocompletion).
  \item Sorting strings.
  \item Insert and lookup cost only depends on the length of the string and it is
    optimal even in the worst case $\mathcal{O}(length)$.
  \end{itemize}
\end{frame}


\subsection{Simple Tries}

\begin{frame}[fragile]
  \frametitle{Structure}
  \begin{columns}
    \begin{column}{0.28\textwidth}
      \begin{center}
        \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{trie-dog.png}
        \\
        \{dog, dogma, doll\}
      \end{center}
    \end{column}
    \begin{column}{0.75\textwidth}  %%<--- here
      Structure of a simple trie.
      \\~\\
\begin{minted}[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    ]{haskell}
     data Trie = Node (Map Char Trie)
               | Leaf 
\end{minted}
      \begin{itemize}
      \item \textbf{Node}: A map to its children.
      \item \textbf{Leaf}: Indicates the end of a word in the set.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Complexity}
  \begin{itemize}
  \item N: Number of elements.
  \item L: String length.
  \item Alphabet size = $\mathcal{O}(1)$.
  \end{itemize}
  \begin{center}
    \begin{tabular}{ll}  
      \toprule
      Operation   & Complexity\\
      \midrule
      Insert/Lookup/Delete      & $\mathcal{O}(L)$    \\
      Prefix lookup & $\mathcal{O}(L)$     \\
      Sort & $\mathcal{O}(NL)$ \\
      \bottomrule
    \end{tabular}
  \end{center}
  The cost of sorting a set of strings using mergesort is $\mathcal{O}(NL\log{N})$
  (assuming that the comparison function has complexity $\mathcal{O}(L)$).
  Therefore, it can be a good idea to use a trie to sort a list of strings.
\end{frame}
\begin{frame}
  \frametitle{Tries vs hash tables}
  Tries are generally \textcolor{red}{slower} than hash tables when performing
  \begin{itemize}
  \item Single key lookups.
  \item Insertions.
  \item Deletions.
  \end{itemize}
  ~\\ 
  Tries are generally \textcolor{ForestGreen}{faster} than hash tables when performing
  \begin{itemize}
  \item Prefix lookups.
  \item Greater than/smaller than lookups.
  \item Ordered iteration.
  \item Merging.
  \end{itemize}
\end{frame}

\subsection{Radix Tries}
\begin{frame}
	\frametitle{Table of Contents}
	\tableofcontents[currentsection]
\end{frame}

\begin{frame}
  \frametitle{Radix Tries}
  A \textcolor{red}{Radix trie} is like a simple trie, but when a node has only
  one child, it is merged with it.
  \\~\\
  The complexity of all operations remains the same. However, in practice, they
  are much more efficient. Specially when dealing with long words or sets of
  words sharing long prefixes.
  \begin{center}
    \includegraphics[width=\textwidth,height=0.4\textheight,keepaspectratio]{radix-7.png}
  \end{center}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Structure}
  \begin{columns}
    \begin{column}{0.28\textwidth}
      \begin{center}
        \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-dog.png}
        \\
        \{dog, dogma, doll\}
      \end{center}
    \end{column}
    \begin{column}{0.75\textwidth}  %%<--- here
      Structure of a Radix trie.
      \\~\\
\begin{minted}[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    ]{haskell}
     type Prefix = String -- type synonym
     data RTrie = Node Prefix (Map Char RTrie)
               | Leaf 
\end{minted}
      \begin{itemize}
      \item \textbf{Node}: A map to its children and the prefix that they share.
      \item \textbf{Leaf}: Indicates the end of a word in the set.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\subsubsection{Step by step example}

\begin{frame}
  \frametitle{Radix trie creation}
  Inserting romane
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-0.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Radix trie creation}
  Inserting romanus
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-1.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Radix trie creation}
  Inserting romulus
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-2.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Radix trie creation}
  Inserting rubens
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-3.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Radix trie creation}
  Inserting ruber
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-4.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Radix trie creation}
  Inserting rubicon
  \begin{center}
    \includegraphics[width=\textwidth,height=0.6\textheight,keepaspectratio]{radix-5.png}
  \end{center}
\end{frame}

\subsection{Empirical comparison}
\begin{frame}
  \frametitle{Empirical comparison}
  Comparing the size of the tries and their average height.
  \begin{center}
    \begin{tabular}{lr|rara}  
      \toprule
      File   & Word count & Trie nodes & Radix Nodes & Trie E[h] & Radix E[h] \\
      \midrule
      t1 & 17866 & 25704 & 13067 & 8.153 & 5.235 \\
      t2 & 61654 & 29626 & 17723 & 8.185 & 5.656 \\
      t3 & 14900 & 21460 & 10702 & 8.134 & 5.129 \\
      t4 & 18056 & 21606 & 11323 & 7.947 & 5.171 \\
      t5 & 2017 & 5654 & 2574 & 7.327 & 4.302 \\
      t6 & 36863 & 23108 & 13653 & 7.893 & 5.560 \\
     \bottomrule
    \end{tabular}
  \end{center}
  The test files have been selected arbitrarily from \url{http://textfiles.com/}
\end{frame}

\section{Bit Tries: Fast integer sets based on Patricia tries}
\begin{frame}
	\frametitle{Table of Contents}
	\tableofcontents[currentsection]
\end{frame}

\begin{frame}
  \frametitle{Fast integer sets and maps}
  In this section we will do an overview of en efficient implementation of
  \textcolor{red}{integer sets}.

  \begin{itemize}
  \item Based on Patricia tries.
  \item Is the standard implementation of integer sets in Haskell. 
  \item Only for finite precision integers (32 or 64 bits).
  \end{itemize}
\end{frame}

\subsection{Structure}

\begin{frame}[fragile]
  \frametitle{Structure}
  Integers are stored in \textcolor{red}{big endian} order (most significant
  bits come first).
  \\~\\
  There are three kinds of nodes:
  \begin{itemize}
  \item Bin: An intermediate node.
  \item Tip: A leaf (can contain multiple elements).
  \item Nil: An empty set.
  \end{itemize}
  \begin{minted}[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    ]{haskell}
     data IntSet = Bin Prefix Mask IntSet IntSet
                 | Tip Prefix BitMap
                 | Nil
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Bin: Intermediate node}
 \begin{minted}[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    ]{haskell}
    type Prefix = Int -- type synonym
    type Mask = Int 
    data IntSet = Bin Prefix Mask IntSet IntSet
                  ...
  \end{minted}
  \begin{itemize}
  \item \textbf{Mask}: The Mask is a power of 2. It is the largest bit position at which
    two elements of the set differ.
  \item \textbf{Prefix}: Is the common high-order bits that all elements share to the
    left of the Mask bit.
  \item The left tree consists of the elements that don't have the mask bit set.
    The right tree is all the elements that do.
  \end{itemize}
%  Invariant: Nil is never found as a child of Bin.
% -- Invariant: The Mask is a power of 2.  It is the largest bit position at which
% --            two elements of the set differ.
% -- Invariant: Prefix is the common high-order bits that all elements share to
% --            the left of the Mask bit.
% -- Invariant: In Bin prefix mask left right, left consists of the elements that
% --            don't have the mask bit set; right is all the elements that do.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Tip: Multi Leaf}
  \begin{minted}[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    ]{haskell}
    type Prefix = Int -- type synonym
    type BitMap = Int 
    data IntSet = ...
                 | Tip Prefix BitMap 
  \end{minted}
  ~\\
  The values of the map represented by a tip are the
  prefix plus the indices of the set bits in the bit map. 
  \begin{itemize}
  \item \textbf{Prefix}: The prefix shared by all the elements in the bitmap.
    The last 6 bits of the prefix are 0.
  \item \textbf{BitMap}: Last 6 bits stored as a bitmap.
  \end{itemize}
  \textbf{Example}:\\195 = $128+64+2+1$ = 0b\textcolor{blue}{11}\textcolor{orange}{000011} would be stored as \texttt{Tip 192 8}. 
% -- Invariant: The Prefix is zero for all but the last 5 (on 32 bit arches) or 6
% --            bits (on 64 bit arches). The values of the map represented by a tip
% --            are the prefix plus the indices of the set bits in the bit map.
% -- A number stored in a set is stored as
% -- * Prefix (all but last 6 bits) and
% -- * BitMap (last 6 bits stored as a bitmask)
% --   Last 6 bits are called a Suffix.
\end{frame}

\subsection{Examples}


\begin{frame}[fragile]
  \frametitle{More Examples}
  Best seen in examples.
  
  \begin{itemize}
  \item \{\} = \texttt{Nil}
  \item \{0\} = \texttt{Tip 0 1}
  \item \{0, 1, 2\} = \texttt{Tip 0 7}  
  \item \{0..63\} = \texttt{Tip 0 18446744073709551615}\\
    18446744073709551615 = $2^{64} - 1$ \\
    \textcolor{red}{64 integers} in a \textcolor{red}{single leaf}!
  \item \{68, 66, 0, 1, 2\} = \texttt{Bin 0 64 (Tip 0 7) (Tip 64 20)}\\
    68 = 0b\textcolor{red}{1}\textcolor{orange}{000100}\\
    66 = 0b\textcolor{red}{1}\textcolor{orange}{000010}\\
    20 = 0b10100
  \item \{1026, 1091\} = \texttt{Bin 1024 64 (Tip 1024 4) (Tip 1088 8)} \\
    1026 = 0b\textcolor{blue}{1000}\textcolor{red}{0}\textcolor{orange}{000010} \\
    1091 = 0b\textcolor{blue}{1000}\textcolor{red}{1}\textcolor{orange}{000011}
    % 1091 = 0b 1000 1 000011
  \end{itemize}
\end{frame}

\subsection{Benchmarks}

\begin{frame}
  \frametitle{Benchmarks}
  \centering
  See the html reports.
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \nocite{*}
  \bibliographystyle{amsalpha}
  \bibliography{biblio.bib}
\end{frame}

\begin{frame}
  \frametitle{References}
  All the code is available at: \url{https://bitbucket.org/janmasrovira/miri-ads-tries/}
\end{frame}
     
\begin{frame}
 \centering
 Thanks
\end{frame}



\end{document}