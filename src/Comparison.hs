module Comparison where

import qualified Trie as T
import qualified RadixTree as R
import Data.List
import Numeric

testFile :: FilePath -> IO (Int, Int, Int, Double, Double)
testFile file = do
  ws <- words <$> readFile file
  let wc = length ws
      t = T.fromList ws
      r = R.fromList ws
      (tn, th) = (T.nodeCount t, T.avgHeight t)
      (rn, rh) = (R.nodeCount r, R.avgHeight r)
  return (wc, tn, rn, th, rh)

files = ["data/t" ++ show i ++ ".txt" | i <- [1..6]]

testFiles :: [String] -> IO String
testFiles = fmap unlines . mapM (fmap showRes . testFile)

showRes :: (Int, Int, Int, Double, Double) -> String
showRes (a,b,c,d,e) = intercalate " & " (map show [a,b,c] ++ map showPrec3 [d, e]) ++ " \\\\"

showPrec3 :: RealFloat a => a -> String
showPrec3 d = showFFloat (Just 3) d ""
